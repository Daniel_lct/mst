// modal

const searchButton = document.querySelector(".header-search__button");
const searchModalWindow = document.querySelector(".modal-search");
const closeModalButton = document.querySelector(".modal-search__close-button");

searchButton.addEventListener("click", function () {
  searchModalWindow.classList.add("modal-search_active");
});
closeModalButton.addEventListener("click", function () {
  searchModalWindow.classList.remove("modal-search_active");
});

// menu dropdown
// подразумевается, что на каждую иконку для открытия есть свой список, актуально только для хедера

const openDropdown = document.querySelectorAll(".header-menu-list__item_drop");
const openDropdownLink = document.querySelectorAll(
    ".header-menu-list__item_drop > a"
);
const dropdownList = document.querySelectorAll(".header-menu-list-drop");

for (let i = 0; i < openDropdown.length; i++) {
  openDropdown[i].addEventListener("mousemove", function () {
    openDropdown[i].style.cssText = "color: blue";
    openDropdownLink[i].style.cssText = "color: blue";
    dropdownList[i].classList.add("header-menu-list-drop_active");
  });
  openDropdown[i].addEventListener("mouseout", function () {
    dropdownList[i].classList.remove("header-menu-list-drop_active");
    openDropdown[i].style.cssText = "";
    openDropdownLink[i].style.cssText = "";
  });
}



// mobile menu

const openMenuButton = document.querySelector(".header-open-menu__button");
const mobileMenuWindow = document.querySelector(".mobile-menu-window");

openMenuButton.addEventListener("click", function () {
  if (mobileMenuWindow.classList.contains("mobile-menu-window_active")) {
    openMenuButton.innerHTML = `
      <svg xmlns="http://www.w3.org/2000/svg" width="24" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
      </svg>`;
    mobileMenuWindow.classList.remove("mobile-menu-window_active");
  } else {
    openMenuButton.innerHTML = `
      <svg xmlns="http://www.w3.org/2000/svg" width="24" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
      </svg>`;
    mobileMenuWindow.classList.add("mobile-menu-window_active");
  }
});

const openDropdownMobile = document.querySelectorAll(
    ".mobile-menu-list__item_drop i"
);
const dropdownListMobile = document.querySelectorAll(".mobile-menu-list-drop");

for (let i = 0; i < openDropdownMobile.length; i++) {
  openDropdownMobile[i].addEventListener("click", function () {
    if (
        dropdownListMobile[i].classList.contains("mobile-menu-list-drop_active")
    ) {
      openDropdownMobile[i].classList.add("bi-chevron-right");
      openDropdownMobile[i].classList.remove("bi-chevron-down");
      dropdownListMobile[i].classList.remove("mobile-menu-list-drop_active");
    } else {
      openDropdownMobile[i].classList.remove("bi-chevron-right");
      openDropdownMobile[i].classList.add("bi-chevron-down");
      dropdownListMobile[i].classList.add("mobile-menu-list-drop_active");
    }
  });
}

const selectLangMobile = document.querySelector(".mobile-menu-action__lang");
const selectLangElementMobile = document.querySelector(
    ".mobile-menu-action__lang ul"
);

selectLangMobile.addEventListener("click", function () {
  selectLangElementMobile.classList.toggle("open");
});

// modal download

const modalDownload = document.querySelector(".modal-download");
const openDownloadButton = document.querySelector(".download-button");
const closeDownloadButton = document.querySelector(".download-close_button");

openDownloadButton.addEventListener("click", function () {
  modalDownload.classList.add("modal-download_active");
});

closeDownloadButton.addEventListener("click", function () {
  modalDownload.classList.remove("modal-download_active");
});


/**
 * Убрать fade effect со слайдера на разрешении от 1024px
 */

$(document).ready(function(){
  if ($(window).width() > 1024){
    $('#recipeCarousel').attr('class', 'carousel slide w-100 pointer-event')
  } else {
    $('#recipeCarousel').attr('class', 'carousel carousel-fade slide w-100 pointer-event')
  }

  $(window).resize(function(){
    if ($(window).width()> 1024){
      $('#recipeCarousel').attr('class', 'carousel slide w-100 pointer-event')
    } else {
      $('#recipeCarousel').attr('class', 'carousel carousel-fade slide w-100 pointer-event')
    }
  })
})

$(document).ready(function(){
  $("#recipeCarousel").carousel({
    interval: 10000,
  });

  $(".carousel .carousel-item").each(function () {
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(":first");
    }
    next.children(":first-child").clone().appendTo($(this));

    for (var i = 0; i < minPerSlide; i++) {
      next = next.next();
      if (!next.length) {
        next = $(this).siblings(":first");
      }

      next.children(":first-child").clone().appendTo($(this));
    }
  });
})

$(document).ready(function () {
  var swiper = new Swiper('.swiper-container', {
    slidesPerView: 1,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      560: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      },
    }
  });
})